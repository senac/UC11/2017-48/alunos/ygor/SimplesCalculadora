package br.com.senac.calculadorasimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
private int NumeroUM;
private int NumeroDois;

public void calcular(){


}}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final TextView txtResultado = (TextView) findViewById(R.id.txtResultado);

        final EditText txtNumeroUm = (EditText) findViewById(R.id.txtNumeroUm);
        final EditText txtNumeroDois = (EditText) findViewById(R.id.Numerodois);

        Button btnsoma = (Button) findViewById(R.id.soma);
        Button btnsub = (Button) findViewById(R.id.sub);
        Button btndiv = (Button) findViewById(R.id.div);
        Button btnmult = (Button) findViewById(R.id.mult);

        btnsoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             valor(txtNumeroUm,txtNumeroDois);
               txtResultado.setText(String.valueOf(soma(NumeroDois,NumeroUM)));

            }


        });
        btnsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valor(txtNumeroDois,txtNumeroUm);
                txtResultado.setText(String.valueOf(sub(NumeroDois,NumeroUM)));
            }
        });
         btndiv.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 valor(txtNumeroUm,txtNumeroDois);
                 txtResultado.setText(String.valueOf(div(NumeroDois,NumeroUM)));
             }
         });
         btnmult.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 valor(txtNumeroUm,txtNumeroDois);
                 txtResultado.setText(String.valueOf(mult(NumeroDois,NumeroUM)));
         });
    }

    public void valor(EditText txtNumeroUm, EditText txtNumeroDois  ){
        NumeroDois =  Integer.parseInt(txtNumeroUm.getText().toString());
        NumeroUM =  Integer.parseInt(txtNumeroDois.getText().toString());

    }
    private int soma(int a, int b){
        return a+b;

    }
    private int sub(int a, int b){
        return a-b;

    }
    private int div(int a, int b){
        return a/b;

    }
    private int mult(int a,int b) {
        return a*b;
    }
}
